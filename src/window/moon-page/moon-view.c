/*
 * moon-view.c
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/* TODO:
 * Error handling for no rise and set
 * Illumination
 */


#include "moon-view.h"
#include <glib/gi18n.h>


struct _PicplannerMoon
{
  GtkBox parent_instance;

  GtkWidget *label_rise_time;
  GtkWidget *label_rise_azimuth;
  GtkWidget *label_upper_time;
  GtkWidget *label_upper_azimuth;
  GtkWidget *label_upper_elevation;
  GtkWidget *label_set_time;
  GtkWidget *label_set_azimuth;
  GtkWidget *label_illumination;
  GtkWidget *label_phase;
};

G_DEFINE_TYPE (PicplannerMoon, picplanner_moon, GTK_TYPE_BOX)


/*
 * Set the index of the rise, upper culmination, set and lowest culmination
 * to present the information in the moon view.
 */
void
picplanner_moon_set_rise_upper_set (PicplannerMoon *moon,
                                    GDateTime *date_time,
                                    double *coordinates_array,
                                    int *index_rise_upper_set)
{
  char *char_rise_time;
  char *char_rise_azimuth;
  char *char_upper_time;
  char *char_upper_azimuth;
  char *char_upper_elevation;
  char *char_set_time;
  char *char_set_azimuth;
  GDateTime *date_time_rise;
  GDateTime *date_time_upper;
  GDateTime *date_time_set;

  date_time_rise = g_date_time_add_minutes (date_time, index_rise_upper_set[0]*24*60/NUM_DATA_POINTS-12*60);
  date_time_upper = g_date_time_add_minutes (date_time, index_rise_upper_set[1]*24*60/NUM_DATA_POINTS-12*60);
  date_time_set = g_date_time_add_minutes (date_time, index_rise_upper_set[2]*24*60/NUM_DATA_POINTS-12*60);

  /*
   * Rise
   */
  if (index_rise_upper_set[0]>0)
    {
      char_rise_time = g_strdup_printf ("%02d:%02d",
                                        g_date_time_get_hour (date_time_rise),
                                        g_date_time_get_minute (date_time_rise));
      char_rise_azimuth = g_strdup_printf ("%s: %.0f\u00B0", _("Azimuth"),
                                           coordinates_array[index_rise_upper_set[0]*2]);
    }
  else
    {
      char_rise_time = g_strdup_printf ("--:--");
      char_rise_azimuth = g_strdup_printf ("%s: -\u00B0", _("Azimuth"));
    }

  /*
   * Upper culmination
   */
  char_upper_time = g_strdup_printf ("%02d:%02d",
                                     g_date_time_get_hour (date_time_upper),
                                     g_date_time_get_minute (date_time_upper));
  char_upper_azimuth = g_strdup_printf ("%s: %.0f\u00B0", _("Azimuth"),
                                        coordinates_array[index_rise_upper_set[1]*2]);
  char_upper_elevation = g_strdup_printf ("%s: %.0f\u00B0", _("Elevation"),
                                        coordinates_array[index_rise_upper_set[1]*2+1]);

  /*
   * Set
   */
  if (index_rise_upper_set[2]>0)
    {
      char_set_time = g_strdup_printf ("%02d:%02d",
                                       g_date_time_get_hour (date_time_set),
                                       g_date_time_get_minute (date_time_set));
      char_set_azimuth = g_strdup_printf ("%s %.0f\u00B0", _("Azimuth"),
                                          coordinates_array[index_rise_upper_set[2]*2]);
    }
  else
    {
      char_set_time = g_strdup_printf ("--:--");
      char_set_azimuth = g_strdup_printf ("%s-\u00B0", _("Azimuth"));
    }

  gtk_label_set_text (GTK_LABEL (moon->label_rise_time), char_rise_time);
  gtk_label_set_text (GTK_LABEL (moon->label_rise_azimuth), char_rise_azimuth);
  gtk_label_set_text (GTK_LABEL (moon->label_upper_time), char_upper_time);
  gtk_label_set_text (GTK_LABEL (moon->label_upper_azimuth), char_upper_azimuth);
  gtk_label_set_text (GTK_LABEL (moon->label_upper_elevation), char_upper_elevation);
  gtk_label_set_text (GTK_LABEL (moon->label_set_time), char_set_time);
  gtk_label_set_text (GTK_LABEL (moon->label_set_azimuth), char_set_azimuth);


  g_date_time_unref (date_time_rise);
  g_date_time_unref (date_time_upper);
  g_date_time_unref (date_time_set);
}


/*
 * Set the illumination percentage of the moon as well as the current
 * phase trend and presents it in the moon view.
 */
void
picplanner_moon_set_illumination_intensity (PicplannerMoon *moon,
                                            double illumination,
                                            char *phase)
{
  char *char_label_illumination;

  char_label_illumination = g_strdup_printf ("%d%%", (int)round(illumination));
  gtk_label_set_text (GTK_LABEL (moon->label_illumination), char_label_illumination);

  gtk_label_set_text (GTK_LABEL (moon->label_phase), phase);
  g_free (char_label_illumination);
}


static void
picplanner_moon_init (PicplannerMoon *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
picplanner_moon_class_init (PicplannerMoonClass *class)
{
  gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (class),
                                               "/de/zwarf/picplanner/window/moon-page/moon-view.ui");
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMoon, label_rise_time);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMoon, label_rise_azimuth);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMoon, label_upper_time);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMoon, label_upper_azimuth);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMoon, label_upper_elevation);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMoon, label_set_time);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMoon, label_set_azimuth);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMoon, label_illumination);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), PicplannerMoon, label_phase);
}

PicplannerMoon *
picplanner_moon_new ()
{
  return g_object_new (PICPLANNER_MOON_TYPE, NULL);
}
