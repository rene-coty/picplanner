/*
 * marker.h
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * Code mainly taken from https://gitlab.gnome.org/GNOME/libshumate/-/blob/main/shumate/shumate-marker.h
 * This is a copy of the 'ShumateMarker' class adjusted for the needs of PicPlanner
 */

#ifndef PICPLANNER_MARKER_H
#define PICPLANNER_MARKER_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define PICPLANNER_TYPE_MARKER picplanner_marker_get_type ()
G_DECLARE_DERIVABLE_TYPE (PicplannerMarker, picplanner_marker, PICPLANNER, MARKER, GtkWidget)

struct _PicplannerMarkerClass
{
  GtkWidgetClass parent_class;
};

void
picplanner_marker_set_location (PicplannerMarker *marker,
                                double           x,
                                double           y,
                                double           x_offset,
                                double           y_offset);

double
picplanner_marker_get_x (PicplannerMarker *marker);

double
picplanner_marker_get_y (PicplannerMarker *marker);

double
picplanner_marker_get_x_offset (PicplannerMarker *marker);

double
picplanner_marker_get_y_offset (PicplannerMarker *marker);

PicplannerMarker
*picplanner_marker_new (void);

GtkWidget
*picplanner_marker_get_child (PicplannerMarker *marker);

void
picplanner_marker_set_child (PicplannerMarker *marker,
                             GtkWidget        *child);

G_END_DECLS

#endif

